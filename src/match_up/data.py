from dataclasses import dataclass
from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base


MIN_LEVEL = 1
MAX_LEVEL = 10
PLAYER_PER_COURT = 4
GUEST_LEVELS = [1, 3, 5]
GUESTS_PER_LEVEL = 4

BASE = declarative_base()


class Player(BASE):
    __tablename__ = "players"
    id = Column("id", Integer, primary_key=True)
    first = Column("first", String)
    last = Column("last", String)
    level = Column("level", Integer)
    team = Column("team", Integer)
    status = Column("status", Integer)
    count = Column("count", Integer)

    def __init__(
        self,
        id: int,
        first: str = "",
        last: str = "",
        level: int = 0,
        team: int = 0,
        status: bool = True,
        count: int = 0,
    ):
        self.id = id
        self.first = first
        self.last = last
        self.status = status
        self.team = team
        self.level = level
        self.count = count

    def __repr__(self):
        team = f" of Team {self.team}" if self.team else ""
        status = "active" if self.status else "inactive"
        return f"{self.first} {self.last} (Level {self.level}){team} is {status} and has played {self.count} games"


class DisplayPlayer:
    id: int
    first: str
    last: str
    status: bool

    def __init__(self, player: Player):
        self.id = int(player.id)
        self.first = str(player.first)
        self.last = str(player.last)
        self.status = bool(player.status)

    def __repr__(self):
        status = "active" if self.status else "inactive"
        return f"{self.first} {self.last} is {status}"


@dataclass
class Team:
    player1: DisplayPlayer
    player2: DisplayPlayer


@dataclass
class Game:
    court: int
    team1: Team
    team2: Team
