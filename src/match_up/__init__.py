from flask import Flask
from waitress import serve
from argparse import ArgumentParser
from importlib.resources import path
from match_up.controller import RoundGenerator


with path("match_up", "static") as p:
    static_folder = p
with path("match_up", "templates") as p:
    template_folder = p
_app = Flask(
    "Match Up! Backend", static_folder=static_folder, template_folder=template_folder
)
_rg = RoundGenerator()


from match_up import routes


def main():
    parser = ArgumentParser()
    parser.add_argument("-d", "--database", help="CSV database", default=None)
    args = parser.parse_args()
    _rg.update_player_list(csv_location=args.database)
    serve(_app, port=80)
