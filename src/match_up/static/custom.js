let minsPerRound = Number(sessionStorage.getItem("minsPerRound")) || 12;
let timerRunningFlag = Number(sessionStorage.getItem("timerRunningFlag")) || 0;
let deadline = Number(sessionStorage.getItem("deadline")) || new Date().getTime();
let clock = 0;

function setTimer(m, s) {
    let timer = document.getElementById("timer-display");
    m = m < 10 ? ("0" + m) : m;
    s = s < 10 ? ("0" + s) : s;
    timer.value = `${m}:${s}`;
}

function setSessionData() {
    sessionStorage.setItem("deadline", deadline);
    sessionStorage.setItem("minsPerRound", minsPerRound);
    sessionStorage.setItem("timerRunningFlag", timerRunningFlag);
}

function resetTimer() {
    clearInterval(clock);
    setTimer(minsPerRound, 0);
    timerRunningFlag = 0;
    setSessionData();
}

function updateTimer(milliseconds) {
    const m = Math.floor((milliseconds % (1000 * 60 * 60)) / (1000 * 60));
    const s = Math.floor((milliseconds % (1000 * 60)) / 1000);
    setTimer(m, s);
}

function refreshTimer() {
    const now = new Date().getTime();
    const millisecondsToDeadline = deadline - now;
    if (millisecondsToDeadline > 0) {
        updateTimer(millisecondsToDeadline);
        return;
    }
    resetTimer();
    const notification = new Notyf({
        duration: 30 * 1000, 
        position: {x: 'center', y: 'top'},
        types: [{
            type: 'success',
            background: 'indianred',
            duration: 1000 * 1000,
            dismissible: true
        }]
    });
    notification.success("Round completed");
}

function startTimer(fresh) {
    if (fresh) {
        now = new Date().getTime();
        const millisecondsToDeadline = minsPerRound * 60 * 1000;
        deadline = now + millisecondsToDeadline;
    }
    timerRunningFlag = 1;
    setSessionData();
    clock = setInterval(refreshTimer, 1000);
}

if (timerRunningFlag) startTimer(0); else resetTimer();

document.getElementById("timer-start").addEventListener('click', ()=> {
    if (timerRunningFlag != 0) return;
    startTimer(1);
});

document.getElementById("timer-stop").addEventListener('click', ()=>{
    resetTimer();
});

document.getElementById("timer-inc").addEventListener('click', ()=> {
    if (timerRunningFlag == 0) {
        minsPerRound += 1;
        resetTimer();
    }
});

document.getElementById("timer-dec").addEventListener('click', ()=> {
    if (timerRunningFlag == 0 && minsPerRound != 0) {
        minsPerRound -= 1;
        resetTimer();
    }
});