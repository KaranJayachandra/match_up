from match_up import _app, _rg
from jinja2 import Environment, PackageLoader
from flask import render_template, request, url_for, Response

re = Environment(loader=PackageLoader("match_up"))


@_app.route("/", methods=["GET"])
def home():
    return re.get_template("index.j2").render(url_for=url_for)


@_app.route("/control", methods=["GET"])
def get_controls():
    return render_template("controls.j2", courts=_rg.courts.get_courts())


@_app.route("/propose", methods=["POST"])
def get_random_games():
    levels = int(request.form["levels"].split()[0])
    team_practice = request.form["team"] == "Teams"
    return render_template(
        "games.j2", games=_rg.propose(levels, team_practice), title="Proposal"
    )


@_app.route("/confirm", methods=["POST"])
def confirm_games():
    return render_template("games.j2", games=_rg.confirm(), title=f"Round: {_rg.round}")


@_app.route("/clear", methods=["POST"])
def clear_games():
    return render_template("games.j2", games=_rg.clear(), title=f"Round: {_rg.round}")


@_app.route("/reset", methods=["POST"])
def reset_games():
    return render_template("games.j2", games=_rg.reset(), title=f"Round: {_rg.round}")


@_app.route("/player-toggle/<player_request>", methods=["POST"])
def toggle_player(player_request):
    id = int(player_request)
    player = _rg.players.toggle_player_status(id)
    t = re.from_string(
        '{% from "macros.j2" import player_button %}{{player_button(player)}}'
    )
    return render_template(t, player=player)


@_app.route("/players", methods=["GET"])
def get_list_of_players():
    regulars, guests = _rg.players.get_all_players()
    return re.get_template("players.j2").render(
        regulars=regulars, guests=guests, url_for=url_for
    )


@_app.route("/reset_players", methods=["POST"])
def reset_all_players():
    _rg.players.reset_all_players()
    response = Response("Players reset!")
    response.headers["HX-Refresh"] = "true"
    return response


@_app.route("/court-toggle/<court_number>", methods=["POST"])
def toggle_court(court_number):
    id = int(court_number)
    status = _rg.courts.toggle_court_status(id)
    t = re.from_string(
        '{% from "macros.j2" import court_button %}{{ court_button(id, status) }}'
    )
    return render_template(t, id=id, status=status)


@_app.route("/courts", methods=["GET"])
def get_list_of_courts():
    return render_template("courts.j2", courts=_rg.courts.get_courts())
