# Match Up

Match Up! is a flask web application to create pairings of players from a pool and assign to a limited number of slots.

Here it is in action:

![Game Page](https://gitlab.com/KaranJayachandra/match_up/-/raw/main/docs/game_screen.png?ref_type=heads)

![Player Page](https://gitlab.com/KaranJayachandra/match_up/-/raw/main/docs/player_screen.png?ref_type=heads)

Some features of this application are:

- Propose pairings to create a round
- Timer to limit the time for each round
- Create pairings based on player level
- Prioritize players with fewer played games
- Blocking spaces that aren't available any more
- Blocking players that aren't available any more
- Regular and guest players

What this application ***IS***:

- Simple: Easy to modify, extend and use

What this application ***IS NOT***:

- Secure: This was designed to be used locally on a machine
- Performant: It was designed for use by a single admin user

## How to install

With [python](https://realpython.com/installing-python/) installed, run the following command preferably in a seperate virtual environment.

### Using pip

```bash
pip install git+https://gitlab.com/KaranJayachandra/match_up.git
```

### How to complile from source

Please install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and [python](https://realpython.com/installing-python/) on your machine before proceeding further. Once installed, open the terminal to a folder of your choosing and run the following commands to setup the application.

```bash
git clone https://gitlab.com/KaranJayachandra/match_up.git
cd match_up
python -m venv .venv
.venv/Scripts/activate
pip install .
```

To run the application, just run:

```bash
match_up
```

The application should be running now in 'localhost', just navigate there using a browser. Use the `--help` option to get an explanation of the application.

## Startup Automation

Create a powershell script with the following content in the folder with the code.

```bash
.venv/Scripts/activate
pythonw match_up
```

Create a task using Task Scheduler to run this script on start up.

## Architectural Design Record

This part of the README focuses on the motivation of why this application was built and contains background information. Please read on in case you want to know more about the technological choices made.

This is a simple application build using [flask](https://flask.palletsprojects.com/en/3.0.x/), [htmx](https://htmx.org/) and [Bulma](https://bulma.io/). Flask acts as the backend that generates HTML responses using the wonderful [Jinja](https://jinja.palletsprojects.com/en/3.1.x/) templating language. Interactivity of the application is done using HTMX using simple GET and POST methods to the backend. The application is styled using the Bulma with its easy to used CSS classes.

- Python was choose to prioritize delivery speed not for performance. More over, the sheer number of Python programmers allows for the application to be easily maintained by others.
- Flask was choosed as it more than enough for the features that I needed instead of Django. The application is to run locally on a laptop and doesn't really need to have complicated features.
- Due to a lack of frontend developers, the simplest UI stack was choosen. HTMX requires no need of JavaScript and was a natural choice. The website needed some basic styling and after looking at a few CSS frameworks, Bulma provided most of the styling. A toasting library, "Notyf" was also used for simple notifications to be sent via some custom javascript code for a simple timer.
